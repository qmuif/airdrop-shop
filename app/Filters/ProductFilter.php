<?php

namespace App\Filters;

use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class ProductFilter extends AbstractFilter
{
    public function name($value)
    {
        $this->query->where('name', 'ilike', '%'.$value.'%');
    }

    public function status($value)
    {
        $this->query->where('status_id', '=', $value);
    }

    public function price($value)
    {
        $this->query->orderBy('price', $value);
    }

    public function discount($value)
    {
        $this->query
            ->join('discounts', 'discounts.id', '=', 'products.discount_id')
            ->orderBy('discounts.sum', $value);
    }

    public function type($value)
    {
        $this->query->where('type_id', '=', $value);
    }


    public function validate()
    {
        $validator = app()->make('validator')->make($this->request->query(), [
            'name' => 'sometimes|string|max:255',
            'status' => 'sometimes|numeric|exists:product_statuses,id',
            'price' => ['sometimes', Rule::in(['asc', 'desc'])],
            'discount' => ['sometimes', Rule::in(['asc', 'desc'])],
            'type' => 'sometimes|numeric|exists:product_types,id',
        ]);
        if ($validator->fails()) {
            throw new ValidationException($validator, response()->json([
                'message' => $validator->messages()
            ]), 422);
        }
    }
}
