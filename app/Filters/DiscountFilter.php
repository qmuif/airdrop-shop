<?php

namespace App\Filters;

use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class DiscountFilter extends AbstractFilter
{
    public function description($value)
    {
        $this->query->where('description', 'ilike', '%'.$value.'%');
    }

    public function sum($value)
    {
        $this->query->orderBy('sum', $value);
    }

    public function from_date_sort($value)
    {
        $this->query->orderBy('from_date', $value);
    }

    public function to_date_sort($value)
    {
        $this->query->orderBy('to_date', $value);
    }

    public function from_date($value){
        $this->query->whereDate('from_date', '>=', $value);
    }

    public function to_date($value){
        $this->query->whereDate('to_date', '<=', $value);
    }

    public function validate()
    {
        $validator = app()->make('validator')->make($this->request->query(), [
            'description' => 'sometimes|string|max:255',
            'sum' => ['sometimes', Rule::in(['asc', 'desc'])],
            'from_date_sort' => ['sometimes', Rule::in(['asc', 'desc'])],
            'to_date_sort' => ['sometimes', Rule::in(['asc', 'desc'])],
            'from_date' => 'required_with:to_date|date_format:Y-m-d',
            'to_date' => 'required_with:from_date|date_format:Y-m-d|after_or_equal:from_date',
        ]);
        if ($validator->fails()) {
            throw new ValidationException($validator, response()->json([
                'message' => $validator->messages()
            ]), 422);
        }
    }
}
