<?php

namespace App\Filters;

use Illuminate\Validation\ValidationException;

abstract class AbstractFilter
{
    protected $request;
    protected $query;

    public function __construct($query, $request)
    {
        $this->query = $query;
        $this->request = $request;
    }

    /**
     * @throws ValidationException
     */
    public function apply()
    {
        $this->validate();
        foreach ($this->request->all() as $filter => $value) {
            if (method_exists($this, $filter)) {
                $this->$filter($value);
            }
        }
        return $this->query;
    }

    /**
     * @throws ValidationException
     */
    abstract function validate();
}
