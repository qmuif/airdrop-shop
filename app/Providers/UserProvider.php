<?php

namespace App\Providers;

use App\Models\User\User;
use Illuminate\Support\ServiceProvider;

class UserProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        User::created(function ($model) {
            $model->cart()->create();
            $model->subscribes()->create();
        });
    }
}
