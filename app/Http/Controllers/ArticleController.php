<?php

namespace App\Http\Controllers;

use App\Http\Resources\Article\ArticleResource;
use App\Http\Resources\Article\SmallArticleResource;
use App\Models\Article;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Validation\ValidationException;

/**
 * @group Новости
 * @authenticated
 */
class ArticleController extends Controller
{
    /**
     * Показать все новости
     *
     * @unauthenticated
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        return SmallArticleResource::collection(Article::with('image')->cursorPaginate(10));
    }

    /**
     * Создать новую новость.
     *
     * @bodyParam title string required
     * @bodyParam body string required
     * @bodyParam image_id integer required
     * @response 201 {"id" : 12}
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request): JsonResponse
    {
        $data = $this->validate($request, [
            'title' => 'required|string|max:255',
            'body' => 'required|string',
            'image_id' => 'sometimes|exists:images,id',
        ]);
        $article = Article::create($data);
        return response()->json(['id' => $article->id], 201);
    }

    /**
     * Показать определенную новость
     *
     * @unauthenticated
     * @urlParam id integer required ID новости
     * @apiResourceCollection App\Http\Resources\Article\ArticleResource
     * @apiResourceModel App\Models\Article
     *
     * @param $id
     * @return ArticleResource
     */
    public function show($id): ArticleResource
    {
        return new ArticleResource(Article::findOrFail($id));
    }

    /**
     * Обновить новость
     *
     * @urlParam id integer required ID новости
     * @bodyParam title string
     * @bodyParam body string
     * @bodyParam image_id integer
     * @response 201 {"id" : 12}
     *
     * @param Request $request
     * @param $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Request $request, $id): JsonResponse
    {
        $article = Article::findOrFail($id);
        $data = $this->validate($request, [
            'title' => 'sometimes|string|max:255',
            'body' => 'sometimes|string',
            'image_id' => 'sometimes|exists:images,id',
        ]);
        $article->fill($data)->save();
        return response()->json([]);
    }

    /**
     * Удалить новость
     *
     * @urlParam id integer required ID новости
     * @response 200
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $article = Article::findOrFail($id);
        $article->delete();
        return response()->json([]);
    }
}
