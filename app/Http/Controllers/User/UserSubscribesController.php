<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * @group Пользователь
 */
class UserSubscribesController extends Controller
{
    /**
     * Получение разрешений пользователя на рассылки
     *
     * @response 200 {"email":true,"sms":false}
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $subscribe = $request->user()->subscrible;
        return response()->json($subscribe->toArray());
    }

    /**
     * Редактирование разрешений пользователя на рассылки
     *
     * @authenticated
     * @bodyParam email boolean
     * @bodyParam sms boolean
     * @response 200
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Request $request): JsonResponse
    {
        $data = $this->validate($request, [
            'email' => 'sometimes|boolean',
            'sms' => 'sometimes|boolean',
        ]);
        $user = $request->user();
        $user->subscrible->fill($data)->save();
        return response()->json([]);
    }
}
