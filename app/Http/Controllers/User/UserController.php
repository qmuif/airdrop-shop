<?php

namespace App\Http\Controllers\User;

use App\Http\Resources\User\UserCollection;
use App\Http\Resources\User\UserResource;
use App\Jobs\SendReminderEmail;
use App\Models\User\RoleUser;
use App\Models\User\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

/**
 * @group Пользователь
 * @authenticated
 */
class UserController extends Controller
{

    /**
     * @return void
     */
    public function __construct()
    {
        $this->middleware('permission:view-users', ['only' => ['index', 'show']]);
    }


    /**
     * Показать всех пользователей
     *
     * @apiResourceCollection App\Http\Resources\User\UserCollection
     * @apiResourceModel App\Models\User\User
     *
     * @return UserCollection
     */
    public function index(): UserCollection
    {
        return new UserCollection(User::cursorPaginate(10));
    }

    /**
     * Регистрация нового пользователя
     *
     * @responseFile status=200 storage/responses/me.get.json
     * @bodyParam email string required Почта для входа в систему Example: email@gmail.com
     * @bodyParam password string required Пароль для входа в систему.
     * @group Аутентификация
     * @unauthenticated
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function registration(Request $request): JsonResponse
    {
        $data = $this->validate($request, [
            'email' => 'unique:users|required|email|max:255',
            'password' => 'required'
        ]);
        $data["password"] = app('hash')->make($data["password"]);
        $user = User::create($data);
        $code = $user->generateConfirmCode();
        $this->dispatch(new SendReminderEmail($user, $code));
        return $this->respondWithToken(auth()->login($user));
    }

    /**
     * Подтверждение email
     *
     * @group Аутентификация
     * @bodyParam code integer required Код подтверждения.
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function confirmEmail(Request $request): JsonResponse
    {
        $user = $request->user();
        $code = $user->confirmCode->code;
        $this->validate($request, [
            'code' => ['required', 'max:999999',
                function ($attribute, $value, $fail) use ($code) {
                    if (+$value !== $code) $fail('Код подтверждения не действителен');
                },
            ],
        ]);
        $user->is_confirmed = true;
        $user->save();
        return response()->json([
            'message' => 'Email адрес успешно подтвержден'
        ]);
    }

    /**
     * Отобразить определенного пользователя
     *
     * @urlParam id integer required ID пользователя.
     * @apiResource App\Http\Resources\User\UserResource
     * @apiResourceModel App\Models\User\User
     * @param int $id
     * @return UserResource
     */
    public function show($id): UserResource
    {
        return new UserResource(User::findOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param RoleUser $roleUser
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param RoleUser $roleUser
     * @return Response
     */
    public function destroy(RoleUser $roleUser)
    {
        //
    }

    /**
     * Получение структуры ответа содержащего токен.
     *
     * @param string $token
     * @return JsonResponse
     */
    protected function respondWithToken(string $token): JsonResponse
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
