<?php

namespace App\Http\Controllers;

use App\Http\Resources\GiftCategoryResource;
use App\Models\GiftCategories;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Validation\ValidationException;

/**
 * @group Подарочные категории продуктов
 * @authenticated
 */
class GiftCategoriesController extends Controller
{
    /**
     * @return void
     */
    public function __construct()
    {
        $this->middleware('permission:add-gift-categories', ['only' => 'store']);
        $this->middleware('permission:edit-gift-categories', ['only' => 'update']);
        $this->middleware('permission:delete-gift-categories', ['only' => 'delete']);
    }

    /**
     * Отобразить все подарочные категории продуктов.
     *
     * @unauthenticated
     * @apiResourceCollection App\Http\Resources\GiftCategoryResource
     * @apiResourceModel App\Models\GiftCategories
     *
     * @return AnonymousResourceCollection
     */
    public function index()
    {
        return GiftCategoryResource::collection(GiftCategories::all());
    }

    /**
     * Создать новую подарочную категорию
     *
     * @bodyParam name string required
     * @response 201 {"id" : 12}
     */
    public function store(Request $request)
    {
        $data = $this->validate($request, [
            'name' => 'required|string|max:255',
        ]);
        $gift_category = GiftCategories::create($data);
        return response()->json(['id' => $gift_category->id], 201);
    }

    /**
     * Отредактировать подарочную категорию
     *
     * @urlParam id integer required ID подарочной категории.
     * @bodyParam name string required
     * @response 200
     *
     * @param Request $request
     * @param $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Request $request, $id)
    {
        $gift_category = GiftCategories::findOrFail($id);
        $data = $this->validate($request, [
            'name' => 'required|string|max:255',
        ]);
        $gift_category->fill($data)->save();
        return response()->json([]);
    }

    /**
     * Удалить подарочную категорию
     *
     * @urlParam id integer required ID скидки
     * @response 200
     *
     * @param $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        $discount = GiftCategories::findOrFail($id);
        $discount->delete();
        return response()->json([]);
    }
}
