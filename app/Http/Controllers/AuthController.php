<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;

/**
 * @group Аутентификация
 * @authenticated
 */
class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['login']]);
    }


    /**
     * Авторизация в системе
     * @unauthenticated
     *
     * @bodyParam email string required Почта для входа в систему Example: email@gmail.com
     * @bodyParam password string required Пароль для входа в систему.
     * @responseFile status=200 storage/responses/responseToken.json
     * @responseField access_token Токен для авторизации.
     * @responseField token_type Тип токена (bearer).
     * @responseField expires_in Время жизни токена в секундах.
     * @response 401 {"error" : "Unauthorized"}
     * @return JsonResponse
     */
    public function login(): JsonResponse
    {
        $credentials = request(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Получение структуры ответа содержащего токен.
     *
     * @param string $token
     * @return JsonResponse
     */
    protected function respondWithToken(string $token): JsonResponse
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

    /**
     * Получение информации об авторизованном пользователе
     *
     * @authenticated
     * @responseFile status=200 storage/responses/me.get.json
     * @responseField user Текущий авторизованный пользователь.
     * @responseField roles Роли пользователя.
     * @responseField permissions Все доступы пользователя (включая доступы через роли).
     * @return JsonResponse
     */
    public function me(): JsonResponse
    {
        return response()->json([
            'user' => auth()->user(),
            'roles' => auth()->payload()->get('roles'),
            'permissions' => auth()->payload()->get('permissions'),
        ]);
    }

    /**
     * Выход из системы
     *
     * @return JsonResponse
     */
    public function logout(): JsonResponse
    {
        auth()->logout();
        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Обновление токена
     *
     * @responseFile status=200 storage/responses/responseToken.json
     * @return JsonResponse
     */
    public function refresh(): JsonResponse
    {
        return $this->respondWithToken(auth()->refresh());
    }
}
