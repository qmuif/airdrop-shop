<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Traits\HasDownloadImage;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

/**
 * @group Загрузка изображений
 * @authenticated
 */
class ImageController extends Controller
{
    use HasDownloadImage;

    /**
     * @return void
     */
    public function __construct()
    {
        $this->middleware('permission:add-products', ['only' => 'store']);
    }

    /**
     * Загрузка изображения на сервер.
     *
     * @bodyParam image file required
     * @bodyParam description string required
     * @response 201 {"id" : 12}
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request): JsonResponse
    {
        $data = $this->validate($request, [
            'image' => 'required|image',
            'description' => 'required|string|min:3,max:50',
        ]);
        $path = $this->saveImage($request, 'image');
        $image = Image::create([
            'path' => $path,
            'description' => $data['description']
        ]);
        return response()->json([
            'id' => $image->id
        ], 201);
    }
}
