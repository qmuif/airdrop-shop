<?php

namespace App\Http\Controllers\Product;

use App\Http\Resources\Product\ProductResource;
use App\Models\Product\Product;
use App\Settings;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Routing\Controller;

/**
 * @group Товар дня
 * @authenticated
 */
class ProductDayController extends Controller
{
    /**
     * Показать товар дня
     *
     * @unauthenticated
     * @apiResource App\Http\Resources\Product\ProductResource
     * @apiResourceModel App\Models\Product\Product
     *
     * @return ProductResource
     */
    public function index(Settings $settings)
    {
        $id = $settings->get('product_day');
        return new ProductResource(Product::findOrfail($id));
    }

    /**
     * Сохранить товар дня
     *
     * @bodyParam product_id integer required
     * @response 201
     *
     * @param \Illuminate\Http\Request $request
     * @param Settings $settings
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request, Settings $settings)
    {
        $data = $this->validate($request, [
            'product_id' => 'required|integer|exists:products,id',
        ]);
        $settings->put('product_day', $data["product_day"]);
        return response()->json([], 201);
    }
}
