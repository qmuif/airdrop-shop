<?php

namespace App\Http\Controllers\Product;

use App\Http\Resources\GiftCategoryResource;
use App\Http\Resources\Product\LandingProductResource;
use App\Http\Resources\Product\ProductCollection;
use App\Http\Resources\Product\ProductResource;
use App\Http\Resources\StatusResource;
use App\Models\GiftCategories;
use App\Models\Product\Product;
use App\Models\Product\ProductStatus;
use App\Settings;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Routing\Controller;

/**
 * @group Продукты
 */
class ProductController extends Controller
{
    /**
     * @return void
     */
    public function __construct()
    {
        $this->middleware('permission:add-products', ['only' => 'store']);
        $this->middleware('permission:edit-products', ['only' => 'update']);
        $this->middleware('permission:delete-products', ['only' => 'destroy']);
    }

    /**
     * Показать список продуктов
     *
     * @param Request $request
     * @return ProductCollection
     * @throws ValidationException
     */
    public function index(Request $request): ProductCollection
    {
        return new ProductCollection(Product::filter($request)->cursorPaginate(20));
    }

    /**
     * Выборка продуктов для стартовой страницы
     * @param Settings $settings
     * @return JsonResponse
     */
    public function landing(Settings $settings): JsonResponse
    {
        $top_sales = LandingProductResource::collection(Product::inRandomOrder()->limit(20)->get());
        $gift_categories = GiftCategoryResource::collection(GiftCategories::with('products')->get());
        $new_products = LandingProductResource::collection(Product::orderBy('created_at', 'desc')->limit(20)->get());
        $promotional_offers = LandingProductResource::collection(
            Product::join('discounts', 'discounts.id', '=', 'products.discount_id')
                ->orderBy('discounts.sum', 'desc')->limit(20)->get()
        );
        $id = $settings->get('product_day');
        $product_day = new LandingProductResource(Product::find($id));
        return response()->json([
            'top_sales' => $top_sales,
            'gift_categories' => $gift_categories,
            'new_products' => $new_products,
            'promotional_offers' => $promotional_offers,
            'product_day' => $product_day,
        ]);
    }

    /**
     * Создать продукт
     *
     * @authenticated
     * @bodyParam name string required
     * @bodyParam price numeric required
     * @bodyParam status_id integer required
     * @bodyParam image_id integer required
     * @bodyParam discount_id integer
     * @bodyParam type_id integer required
     * @response 201 {"id" : 12}
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request): JsonResponse
    {
        $data = $this->validate($request, [
            'name' => 'required|string|min:3|max:255',
            'description' => 'required|string|min:20',
            'price' => 'required|numeric|between:1,9999999999.99|regex:/^\d*(\.\d{2})?$/',
            'status_id' => 'required|integer|exists:product_statuses,id',
            'image_id' => 'required|integer|exists:images,id',
            'discount_id' => 'sometimes|integer|exists:discounts,id',
            'type_id' => 'required|integer|exists:product_types,id',
        ]);
        $product = Product::create($data);
        return response()->json(['id' => $product->id], 201);
    }

    /**
     * Показать указанный продукт
     *
     * @urlParam id integer required ID продукта.
     * @param int $id Id пользователя
     * @return ProductResource
     */
    public function show($id): ProductResource
    {
        return new ProductResource(Product::with('images')->findOrfail($id));
    }

    /**
     * Обновить указанный продукт
     *
     * @authenticated
     * @urlParam id integer required ID продукта.
     * @bodyParam name string required
     * @bodyParam price numeric
     * @bodyParam status_id integer
     * @bodyParam image_id integer
     * @bodyParam discount_id integer
     * @bodyParam type_id integer
     * @response 200
     *
     * @param Request $request
     * @param $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Request $request, $id): JsonResponse
    {
        $product = Product::findOrFail($id);
        $data = $this->validate($request, [
            'name' => 'sometimes|string|min:3|max:255',
            'description' => 'sometimes|string|min:20',
            'price' => 'sometimes|numeric|between:1,9999999999.99|regex:/^\d*(\.\d{2})?$/',
            'status_id' => 'sometimes|exists:product_statuses,id',
            'image_id' => 'sometimes|exists:images,id',
            'discount_id' => 'sometimes|nullable|exists:discounts,id',
            'type_id' => 'sometimes|exists:product_types,id',
        ]);
        $product->fill($data);
        $product->save();
        return response()->json([]);
    }

    /**
     * Удалить продукт
     *
     * @authenticated
     * @urlParam id integer required ID продукта.
     * @param $id
     * @return JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        $product = Product::findOrFail($id);
        $product->delete();
        return response()->json([]);
    }

    /**
     * Получить справочник статусов продукта
     */
    public function statuses(): AnonymousResourceCollection
    {
        return StatusResource::collection(ProductStatus::all());
    }
}
