<?php

namespace App\Http\Controllers\Product;

use App\Models\Product\Product;
use App\Traits\HasDownloadImage;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Routing\Controller;

/**
 * @group Изображения продуктов
 * @authenticated
 */
class ProductImagesController extends Controller
{
    use HasDownloadImage;
    /**
     * @return void
     */
    public function __construct()
    {
        $this->middleware('permission:edit-products');
    }

    /**
     * Загрузить новое изображение
     *
     * @urlParam $product_id integer required ID продукта.
     * @bodyParam image file required
     * @bodyParam description string required
     * @response 201 {"id" : 12}
     *
     * @param Request $request
     * @param $product_id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request, $product_id)
    {
        $product = Product::findOrFail($product_id);
        $data = $this->validate($request, [
            'image' => 'required|image',
            'description' => 'required|string|min:3,max:255',
        ]);
        $path = $this->saveImage($request, 'image');
        $product->images()->create([
            'path' => $path,
            'description' => $data['description']
        ]);
        return response()->json([], 201);
    }


    /**
     * Удалить изображение у продукта
     *
     * @urlParam $product_id integer required ID продукта.
     * @urlParam $image_id integer required ID изображения продукта.
     *
     * @param Request $request
     * @param int $product_id
     * @param int $image_id
     * @return JsonResponse
     */
    public function destroy(Request $request, int $product_id, int $image_id)
    {
        $product = Product::findOrFail($product_id);
        $product_image = $product->images()->get()->find($image_id);
        if (!$product_image) {
            return response()->json([], 404);
        }
        if(File::exists(app()->storagePath('app/').$product_image->path)){
            File::delete(app()->storagePath('app/').$product_image->path);
        }
        $product_image->delete();
        return response()->json([]);
    }
}
