<?php

namespace App\Http\Controllers\Product;

use App\Http\Resources\ProductTypeResource;
use App\Models\Product\Product;
use App\Models\Product\ProductType;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Routing\Controller;

/**
 * @group Типы продуктов
 * @authenticated
 */
class ProductTypeController extends Controller
{

    /**
     * @return void
     */
    public function __construct()
    {
        $this->middleware('permission:add-products-types', ['only' => 'store']);
        $this->middleware('permission:edit-products-types', ['only' => 'update']);
        $this->middleware('permission:delete-products-types', ['only' => 'destroy']);
    }

    /**
     * Показать список типов продуктов
     * @unauthenticated
     *
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        return ProductTypeResource::collection(ProductType::all());
    }

    /**
     * Создать тип продукта
     *
     * @bodyParam name string required
     * @bodyParam description string
     * @response 200 {"id" : 12}
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request): JsonResponse
    {
        $data = $this->validate($request, [
            'name' => 'required|string|min:3|max:255',
            'description' => 'sometimes|string|min:3|max:255',
        ]);
        $product_type = ProductType::create($data);
        return response()->json(['id'=>$product_type->id], 201);
    }

    /**
     * Обновить указанный тип продукта
     *
     * @urlParam id integer required ID типа продукта.
     * @bodyParam name string required
     * @bodyParam description string
     * @response 200
     *
     * @param Request $request
     * @param $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Request $request, $id): JsonResponse
    {
        $product_type = ProductType::findOrFail($id);
        $data = $this->validate($request, [
            'name' => 'sometimes|string|min:3|max:255',
            'description' => 'sometimes|string|min:3|max:255',
        ]);
        $product_type->fill($data)->save();
        return response()->json([]);
    }

    /**
     * Удалить тип продукта
     *
     * @urlParam id integer required ID типа продукта.
     * @response 200
     *
     * @param $id
     * @return JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        $product_type = ProductType::findOrFail($id);
        $products = Product::where('type_id', '=', $id)->first();
        if($products) {
            return response()->json([
                'message' => "Имеются продукты с данным типом, укажите им другой тип и попробуйте снова."
            ], 400);
        }
        $product_type->delete();
        return response()->json([]);
    }
}
