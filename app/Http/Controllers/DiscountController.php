<?php

namespace App\Http\Controllers;

use App\Http\Resources\DiscountResource;
use App\Models\Discount;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Validation\ValidationException;

/**
 * @group Скидки
 * @authenticated
 */
class DiscountController extends Controller
{
    /**
     * @return void
     */
    public function __construct()
    {
        $this->middleware('permission:view-discounts', ['only' => 'index']);
        $this->middleware('permission:add-discounts', ['only' => 'store']);
        $this->middleware('permission:edit-discounts', ['only' => 'update']);
        $this->middleware('permission:delete-discounts', ['only' => 'delete']);
    }

    /**
     * Показать список скидок
     *
     * @apiResourceCollection App\Http\Resources\DiscountResource
     * @apiResourceModel App\Models\Discount
     *
     * @return AnonymousResourceCollection
     * @throws ValidationException
     */
    public function index(Request $request)
    {
        return DiscountResource::collection(Discount::filter($request)->cursorPaginate(10));
    }

    /**
     * Создать новую скидку
     *
     * @bodyParam description string required
     * @bodyParam sum numeric required
     * @bodyParam status_id integer required
     * @bodyParam from_date date required
     * @bodyParam to_date date required
     * @response 201 {"id" : 12}
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $data = $this->validate($request, [
            'description' => 'sometimes|string|max:255',
            'sum' => 'required|numeric|between:1,9999999999.99|regex:/^\d*(\.\d{2})?$/',
            'from_date' => 'required|date_format:Y-m-d',
            'to_date' => 'required|date_format:Y-m-d|after_or_equal:from_date',
        ]);
        $discount = Discount::create($data);
        return response()->json(['id' => $discount->id], 201);
    }

    /**
     * Отредактировать скидку
     *
     * @urlParam id integer required ID скидки.
     * @bodyParam description string
     * @bodyParam sum numeric
     * @bodyParam from_date date
     * @bodyParam to_date date
     * @response 200
     *
     * @param Request $request
     * @param $id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Request $request, $id)
    {
        $discount = Discount::findOrFail($id);
        $data = $this->validate($request, [
            'description' => 'sometimes|nullable|string|max:255',
            'sum' => 'sometimes|numeric|between:1,9999999999.99|regex:/^\d*(\.\d{2})?$/',
            'from_date' => 'required_with:to_date|date_format:Y-m-d',
            'to_date' => 'required_with:from_date|date_format:Y-m-d|after_or_equal:from_date',
        ]);
        $discount->fill($data);
        $discount->save();
        return response()->json([]);
    }

    /**
     * Удалить скидку
     *
     * @urlParam id integer required ID скидки
     * @response 200
     *
     * @param $id
     * @return JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        $discount = Discount::findOrFail($id);
        $discount->delete();
        return response()->json([]);
    }
}
