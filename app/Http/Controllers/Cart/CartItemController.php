<?php

namespace App\Http\Controllers\Cart;

use App\Models\CartItem;
use App\Models\Product\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * @group Корзина
 * @authenticated
 */
class CartItemController extends BaseController
{
    /**
     * Добавить продукт в корзину
     *
     * @urlParam $product_id integer required ID продукта.
     * @response 201
     *
     * @param int $product_id Id продукта
     * @return JsonResponse
     */
    public function addItem(int $product_id): JsonResponse
    {
        $user = auth()->user();
        Product::findOrFail($product_id);
        $item = CartItem::firstOrCreate([
            'cart_id' => $user->cart()->first()->id,
            'product_id' => $product_id,
        ]);
        $item->quantity = $item->wasRecentlyCreated ? 1 : $item->quantity + 1;
        $item->save();
        return response()->json([], 201);
    }

    /**
     * Обновить указанный продукт в корзине
     *
     * @urlParam $product_id integer required ID продукта.
     * @bodyParam quantity integer required
     * @response 200
     *
     * @param Request $request
     * @param int $product_id
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Request $request, int $product_id): JsonResponse
    {
        $user = $request->user();
        $validateData = $this->validate($request, [
            'quantity' => 'required|integer|numeric|min:1'
        ]);
        Product::findOrFail($product_id);
        $item = CartItem::firstOrCreate([
            'cart_id' => $user->cart()->first()->id,
            'product_id' => $product_id,
        ]);
        $item->quantity = $validateData["quantity"];
        $item->save();
        return response()->json([], 200);
    }

    /**
     * Удалить указанный продукт из корзины
     *
     * @urlParam $product_id integer required ID продукта.
     * @response 200
     *
     * @param Request $request
     * @param int $product_id
     * @return JsonResponse
     */
    public function destroy(Request $request, int $product_id): JsonResponse
    {
        $user = $request->user();
        CartItem::findOrFail([
            'cart_id' => $user->cart()->first()->id,
            'product_id' => $product_id,
        ])->first()->delete();
        return response()->json([], 200);
    }
}
