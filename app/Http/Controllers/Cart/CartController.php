<?php

namespace App\Http\Controllers\Cart;

use App\Http\Resources\CartResource;
use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * @group Корзина
 * @authenticated
 */
class CartController extends BaseController
{
    /**
     * Возвращаем корзину пользователя
     *
     * @apiResource App\Http\Resources\CartResource
     * @apiResourceModel App\Models\Cart
     *
     * @return CartResource
     */
    public function index(): CartResource
    {
        $cart = auth()->user()->cart()->with('products')->first();
        return new CartResource($cart);
    }
}
