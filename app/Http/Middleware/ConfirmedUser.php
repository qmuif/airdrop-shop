<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ConfirmedUser
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!$request->user()->is_confirmed) {
            return response()->json([
                'message' => 'Не подтвержден номер телефона или email'
            ], 403);
        }
        return $next($request);
    }
}
