<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class PermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param \Closure $next
     * @param null $permission
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $permission)
    {
        if($permission !== null && !auth()->user()->hasPermission($permission)) {
            return response()->json([
                'message' => 'У вас нет прав доступа'
            ], 403);
        }
        return $next($request);
    }
}
