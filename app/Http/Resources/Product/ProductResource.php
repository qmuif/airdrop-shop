<?php

namespace App\Http\Resources\Product;

use App\Http\Resources\DiscountResource;
use App\Http\Resources\Image\ImageResource;
use App\Http\Resources\ProductTypeResource;
use App\Http\Resources\StatusResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'price' => $this->price,
            'discounted_price' => $this->getDiscountedPrice(),
            'status' => new StatusResource($this->status),
            'discount' => new DiscountResource($this->discount),
            'type' => new ProductTypeResource($this->type),
            'images' => ImageResource::collection(($this->whenLoaded('images'))),
            'image' => new ImageResource($this->image),
            'quantity' => $this->when($this->pivot, isset($this->pivot) ? $this->pivot->quantity : null)
        ];
    }
}
