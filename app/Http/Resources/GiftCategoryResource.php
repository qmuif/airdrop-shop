<?php

namespace App\Http\Resources;

use App\Http\Resources\Product\LandingProductResource;
use Illuminate\Http\Resources\Json\JsonResource;

class GiftCategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'products' => LandingProductResource::collection(($this->whenLoaded('products')))
        ];
    }
}
