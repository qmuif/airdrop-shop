<?php

namespace App\Mail;

use App\Models\User\User;
use Illuminate\Mail\Mailable;

class ConfirmEmail extends Mailable
{

    /**
     * Код подтверждения
     *
     * @var User
     */
    protected $code;

    /**
     *
     * @return void
     */
    public function __construct($code)
    {
        $this->code = $code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('confirm-email', ['code' => $this->code]);
    }
}
