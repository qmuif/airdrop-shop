<?php

namespace App\Models\User;

use App\Models\Cart;
use App\Traits\HasRolesAndPermissions;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Laravel\Lumen\Auth\Authorizable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Model implements AuthenticatableContract, JWTSubject
{
    use Authenticatable, Authorizable, HasFactory, HasRolesAndPermissions;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'email', 'phone', 'role', 'password', 'is_confirmed'
    ];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public $timestamps = false;


    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [
            'roles' => $this->roles()->get()->pluck('name'),
            'permissions' => collect($this->permissionThroughRole())->pluck('name'),
        ];
    }

    public function confirmCode(): HasOne
    {
        return $this->hasOne('App\Models\User\UserConfirmCode');
    }

    public function cart()
    {
        return $this->hasOne(Cart::class);
    }

    public function subscribes(): HasOne
    {
        return $this->hasOne(UserSubscribes::class);
    }


    public function generateConfirmCode()
    {
        $code = mt_rand(100000, 999999);
        $this->confirmCode()->create([
            'code' => $code
        ]);
        return $code;
    }
}
