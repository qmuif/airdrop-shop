<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    protected $hidden = ['pivot'];

    public $timestamps = false;

    public function permissions()
    {
        return $this->belongsToMany(Permission::class,'role_permissions');
    }
}
