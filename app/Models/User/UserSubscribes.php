<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class UserSubscribes extends Model
{
    protected $fillable = ['email', 'sms'];
    public $timestamps = null;
}
