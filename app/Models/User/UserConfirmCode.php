<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserConfirmCode extends Model
{
    use HasFactory;

    protected $fillable = [
        'id', 'user_id', 'code'
    ];


}
