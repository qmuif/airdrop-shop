<?php

namespace App\Models;

use App\Models\Product\Product;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use App\Filters\DiscountFilter;
use Illuminate\Validation\ValidationException;

class Discount extends Model
{
    use HasFactory;

    protected $fillable = ['sum', 'description', 'from_date', 'to_date'];

    public $timestamps = false;

    /**
     * Получить все продукты с данной скидкой
     */
    public function products(): HasMany
    {
        return $this->hasMany(Product::Class);
    }

    /**
     * @throws ValidationException
     */
    static function filter($request)
    {
        $filter = new DiscountFilter(static::query(), $request);
        return $filter->apply();
    }
}
