<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ProductStatus extends Model
{
    use HasFactory;

    const STATUS_OUT_OF_STOC = 1;
    const STATUS_IN_STOC = 2;
    const STATUS_RUNNING_LOW = 3;

    protected $fillable = ['name', 'description'];

    public $timestamps = false;

    /**
     * Получить все продукты определенного статуса
     */
    public function products(): HasMany
    {
        return $this->hasMany(Product::Class);
    }
}
