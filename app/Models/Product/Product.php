<?php

namespace App\Models\Product;

use App\Filters\ProductFilter;
use App\Models\Discount;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Validation\ValidationException;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'price',
        'status_id',
        'image_id',
        'discount_id',
        'type_id',
        'created_at'
    ];

    const CREATED_AT = 'created_at';
    public $timestamps = false;


    /**
     * Получение цены со скидкой
     * @return float
     */
    public function getDiscountedPrice()
    {
        return round(
            $this->discount ?
                $this->price - $this->discount->sum :
                $this->price,
            2);
    }

    /**
     * @throws ValidationException
     */
    static function filter($request)
    {
        $filter = new ProductFilter(static::query(), $request);
        return $filter->apply();
    }


    /**
     * Получить статус продукта
     */
    public function status(): BelongsTo
    {
        return $this->belongsTo(ProductStatus::class);
    }


    /**
     * Получить тип продукта
     */
    public function type(): BelongsTo
    {
        return $this->belongsTo(ProductType::class);
    }

    /**
     * Получить скидку
     */
    public function discount(): BelongsTo
    {
        return $this->belongsTo(Discount::class);
    }

    /**
     * Получить изображения товара
     */
    public function images()
    {
        return $this->belongsToMany(
            'App\Models\Image',
            'product_images'
        );
    }

    /**
     * Получить основное изображение товара
     */
    public function image()
    {
        return $this->belongsTo(
            'App\Models\Image',
        );
    }

    public function gift_categories() {
        return $this->belongsToMany(
            'App\Models\Image', 'products_gift_categories'
        );
    }
}
