<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'body', 'image_id'];

    public $timestamps = ['created_at', 'updated_at'];

    public function image() {
        return $this->belongsTo('App\Models\Image');
    }
}
