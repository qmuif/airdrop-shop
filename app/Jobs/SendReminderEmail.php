<?php

namespace App\Jobs;

use App\Mail\ConfirmEmail;
use App\Models\User\User;

class SendReminderEmail extends Job
{

    /**
     * Сущность пользователя.
     *
     * @var User
     */
    protected $user;


    /**
     * Код подтверждения почты.
     *
     * @var string
     */
    protected $code;

    public $queue = 'high';

    /**
     * Create a new job instance.
     *
     * @param User $user
     * @param $code
     */
    public function __construct(User $user, $code)
    {
        $this->user = $user;
        $this->code = $code;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        app()->make('mailer')->to($this->user)->send((new ConfirmEmail($this->code)));
    }
}
