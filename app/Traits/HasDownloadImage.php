<?php

namespace App\Traits;

trait HasDownloadImage
{
    public function saveImage($request, $value_name){
        $image = $request->file($value_name);
        $date = getdate(time());
        $path = sprintf('/public/images/%d/%d/%d', $date['year'], $date['mon'], $date['mday']);
        $filename = time().".".$image->getClientOriginalExtension();
        return $image->storeAs($path, $filename);
    }
}
