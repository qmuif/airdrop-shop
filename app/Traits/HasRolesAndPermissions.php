<?php

namespace App\Traits;
use App\Models\User\Permission;
use App\Models\User\Role;

trait HasRolesAndPermissions
{
    /**
     * Связь с ролями
     * @return mixed
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class,'role_users');
    }

    /**
     * Связь с доступами
     * @return mixed
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class,'user_permissions');
    }

    /**
     * Получаем все доступы пользователя через роли
     *
     * Доступы полученные прямым путем user_permissions
     * И доступы полученные через роли role_users -> role_permissions
     * @return mixed
     */
    public function permissionThroughRole()
    {
        $user_permissions = Permission::select('permissions.id', 'permissions.name')
            ->leftJoin('user_permissions', 'permissions.id', '=', 'user_permissions.permission_id')
            ->where('user_permissions.user_id', '=', $this->id)->get();

        $user_roles_permissions = Permission::select('permissions.id', 'permissions.name')
            ->leftJoin('role_permissions', 'permissions.id', '=', 'role_permissions.permission_id')
            ->leftJoin('role_users', 'role_permissions.role_id', '=', 'role_users.role_id')
            ->where('role_users.user_id', '=', $this->id)->get();

        $data = $user_permissions->merge($user_roles_permissions);
        return $data->all();
    }

    /**
     * @TODO доработки
     * @param mixed ...$roles
     * @return bool
     */
    public function hasRole(...$roles) {
        $user_roles = auth()->payload()->get('roles');
        foreach ($roles as $role) {
            if (in_array($role, $user_roles)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Проверяем имеет ли авторизованный пользователь переданный доступ
     * @param $permission
     * @return bool
     */
    public function hasPermission($permission)
    {
        return in_array($permission, auth()->payload()->get('permissions'));
    }


    /**
     * @TODO доработки
     * @param mixed ...$permissions
     * @return $this
     */
    public function givePermissionsTo(... $permissions)
    {
        $permissions = $this->getAllPermissions($permissions);
        if($permissions === null) {
            return $this;
        }
        $this->permissions()->saveMany($permissions);
        return $this;
    }

    /**
     * @TODO доработки
     * @param mixed ...$permissions
     * @return $this
     */
    public function deletePermissions(... $permissions )
    {
        $permissions = $this->getAllPermissions($permissions);
        $this->permissions()->detach($permissions);
        return $this;
    }

    /**
     * @TODO доработки
     * @param mixed ...$permissions
     * @return HasRolesAndPermissions
     */
    public function refreshPermissions(... $permissions )
    {
        $this->permissions()->detach();
        return $this->givePermissionsTo($permissions);
    }
}
