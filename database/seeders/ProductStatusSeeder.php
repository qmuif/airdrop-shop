<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_statuses')->insert([[
            'id' => 1,
            'name' => 'Нет на складе',
            'description' => 'Товар отсутствует'
        ],[
            'id' => 2,
            'name' => 'Есть в наличии',
            'description' => 'Товар в достатке'
        ],[
            'id' => 3,
            'name' => 'Заканчивается',
            'description' => 'Осталось мало товаров'
        ]]);
    }
}
