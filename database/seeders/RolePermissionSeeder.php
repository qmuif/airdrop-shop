<?php

namespace Database\Seeders;

use App\Models\User\Permission;
use App\Models\User\Role;
use App\Models\User\RolePermission;
use Illuminate\Database\Seeder;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_id = Role::where('name', '=', 'Модератор')->first()->id;
        RolePermission::insert([
            ['role_id' => $role_id, 'permission_id' => Permission::where('name', '=', 'view-users')->first()->id],
            ['role_id' => $role_id, 'permission_id' => Permission::where('name', '=', 'add-users')->first()->id],
            ['role_id' => $role_id, 'permission_id' => Permission::where('name', '=', 'edit-users')->first()->id],
            ['role_id' => $role_id, 'permission_id' => Permission::where('name', '=', 'delete-users')->first()->id],
        ]);
    }
}
