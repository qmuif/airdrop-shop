<?php

namespace Database\Seeders;

use App\Models\GiftCategories;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GiftCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        GiftCategories::insert([
            ['name' => 'Детям'],
            ['name' => 'Девушкам'],
            ['name' => 'Парням'],
        ]);

        DB::table('products_gift_categories')->insert([
           ['gift_categories_id' => 1, 'product_id' => 1],
           ['gift_categories_id' => 1, 'product_id' => 2],
           ['gift_categories_id' => 1, 'product_id' => 3],
           ['gift_categories_id' => 1, 'product_id' => 4],
           ['gift_categories_id' => 1, 'product_id' => 5],

           ['gift_categories_id' => 2, 'product_id' => 6],
           ['gift_categories_id' => 2, 'product_id' => 7],
           ['gift_categories_id' => 2, 'product_id' => 8],
           ['gift_categories_id' => 2, 'product_id' => 9],
           ['gift_categories_id' => 2, 'product_id' => 10],

           ['gift_categories_id' => 3, 'product_id' => 11],
           ['gift_categories_id' => 3, 'product_id' => 12],
           ['gift_categories_id' => 3, 'product_id' => 13],
           ['gift_categories_id' => 3, 'product_id' => 14],
           ['gift_categories_id' => 3, 'product_id' => 15],
        ]);
    }
}
