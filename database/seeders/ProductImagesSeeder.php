<?php

namespace Database\Seeders;

use App\Models\Product\ProductImages;
use Illuminate\Database\Seeder;

class ProductImagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductImages::factory()->count(50)->create();
    }
}
