<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            UserConfirmCodeSeeder::class,
            PermissionsSeeder::class,
            UserPermissionSeeder::class,
            RoleSeeder::class,
            RolePermissionSeeder::class,
            RoleUserSeeder::class,
            ImageSeeder::class,
            DiscountSeeder::class,
            ProductStatusSeeder::class,
            ProductTypeSeeder::class,
            ProductSeeder::class,
            ProductImagesSeeder::class,
            CartItemSeeder::class,
            GiftCategoriesSeeder::class,
            SettingsSeeder::class,
            ArticleSeeder::class,
        ]);
    }
}
