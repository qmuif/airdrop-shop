<?php

namespace Database\Seeders;

use App\Models\User\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->count(100)->create();

        User::create([
            'name' => 'Админ Админович',
            'email' => 'patlakh.sergey@gmail.com',
            'password' => '$2y$10$QYavDo09ZjoyukCjB7c4MuARPsVfk1Cr5Bdxm22mJC5jxPsrxGh9a',
            'is_confirmed' => true
        ]);
    }
}
