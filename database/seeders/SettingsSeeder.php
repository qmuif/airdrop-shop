<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Valuestore\Valuestore;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = Valuestore::make(storage_path('app/settings.json'));
        $settings->put('product_day', 1);
    }

}
