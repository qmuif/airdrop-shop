<?php

namespace Database\Seeders;

use App\Models\User\Permission;
use Illuminate\Database\Seeder;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::insert([
            ['name' => 'view-users', 'description' => 'Просмотр пользователей'],
            ['name' => 'add-users', 'description' => 'Добавление пользователей'],
            ['name' => 'edit-users', 'description' => 'Редактирование пользователей'],
            ['name' => 'delete-users', 'description' => 'Удаление пользователей'],

            ['name' => 'view-role', 'description' => 'Просмотр ролей'],
            ['name' => 'add-role', 'description' => 'Добавление ролей'],
            ['name' => 'edit-role', 'description' => 'Редактирование ролей'],
            ['name' => 'delete-role', 'description' => 'Удаление ролей'],

            ['name' => 'view-users-role', 'description' => 'Просмотр ролей пользователей'],
            ['name' => 'add-users-role', 'description' => 'Добавление ролей пользователей'],
            ['name' => 'edit-users-role', 'description' => 'Редактирование ролей пользователей'],
            ['name' => 'delete-users-role', 'description' => 'Удаление ролей пользователей'],

            ['name' => 'view-products', 'description' => 'Просмотр продуктов'],
            ['name' => 'add-products', 'description' => 'Добавление продуктов'],
            ['name' => 'edit-products', 'description' => 'Редактирование продуктов'],
            ['name' => 'delete-products', 'description' => 'Удаление продуктов'],

            ['name' => 'add-images', 'description' => 'Загрузка изображений'],
            ['name' => 'delete-images', 'description' => 'Удаление изображений'],

            ['name' => 'view-products-images', 'description' => 'Просмотр изображений продуктов'],
            ['name' => 'add-products-images', 'description' => 'Добавление изображений продуктов'],
            ['name' => 'edit-products-images', 'description' => 'Редактирование изображений продуктов'],
            ['name' => 'delete-products-images', 'description' => 'Удаление изображений продуктов'],

            ['name' => 'view-products-status', 'description' => 'Просмотр статуса продуктов'],
            ['name' => 'add-products-status', 'description' => 'Добавление статуса продуктов'],
            ['name' => 'edit-products-status', 'description' => 'Редактирование статуса продуктов'],
            ['name' => 'delete-products-status', 'description' => 'Удаление статуса продуктов'],

            ['name' => 'view-products-types', 'description' => 'Просмотр типов продуктов'],
            ['name' => 'add-products-types', 'description' => 'Добавление типов продуктов'],
            ['name' => 'edit-products-types', 'description' => 'Редактирование типов продуктов'],
            ['name' => 'delete-products-types', 'description' => 'Удаление типов продуктов'],

            ['name' => 'view-discounts', 'description' => 'Просмотр скидок'],
            ['name' => 'add-discounts', 'description' => 'Добавление скидок'],
            ['name' => 'edit-discounts', 'description' => 'Редактирование скидок'],
            ['name' => 'delete-discounts', 'description' => 'Удаление скидок'],

            ['name' => 'view-gift-categories', 'description' => 'Просмотр подарочных категорий продуктов'],
            ['name' => 'add-gift-categories', 'description' => 'Добавление подарочных категорий продуктов'],
            ['name' => 'edit-gift-categories', 'description' => 'Редактирование подарочных категорий продуктов'],
            ['name' => 'delete-gift-categories', 'description' => 'Удаление подарочных категорий продуктов'],

            ['name' => 'view-articles', 'description' => 'Просмотр новостей'],
            ['name' => 'add-articles', 'description' => 'Добавление новостей'],
            ['name' => 'edit-articles', 'description' => 'Редактирование новостей'],
            ['name' => 'delete-articles', 'description' => 'Удаление новостей'],
        ]);
    }
}
