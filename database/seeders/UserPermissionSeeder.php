<?php

namespace Database\Seeders;

use App\Models\User\Permission;
use App\Models\User\User;
use App\Models\UserPermission;
use Illuminate\Database\Seeder;

class UserPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_id = User::where('email', '=', 'patlakh.sergey@gmail.com')->first()->id;
        UserPermission::insert([
            ['user_id' => $user_id, 'permission_id' => Permission::where('name', '=', 'view-users')->first()->id],
            ['user_id' => $user_id, 'permission_id' => Permission::where('name', '=', 'add-users')->first()->id],
            ['user_id' => $user_id, 'permission_id' => Permission::where('name', '=', 'edit-users')->first()->id],
            ['user_id' => $user_id, 'permission_id' => Permission::where('name', '=', 'delete-users')->first()->id],
        ]);
        UserPermission::insert([
            ['user_id' => $user_id, 'permission_id' => Permission::where('name', '=', 'view-products')->first()->id],
            ['user_id' => $user_id, 'permission_id' => Permission::where('name', '=', 'add-products')->first()->id],
            ['user_id' => $user_id, 'permission_id' => Permission::where('name', '=', 'edit-products')->first()->id],
            ['user_id' => $user_id, 'permission_id' => Permission::where('name', '=', 'delete-products')->first()->id],
        ]);
        UserPermission::insert([
            ['user_id' => $user_id, 'permission_id' => Permission::where('name', '=', 'view-products-types')->first()->id],
            ['user_id' => $user_id, 'permission_id' => Permission::where('name', '=', 'add-products-types')->first()->id],
            ['user_id' => $user_id, 'permission_id' => Permission::where('name', '=', 'edit-products-types')->first()->id],
            ['user_id' => $user_id, 'permission_id' => Permission::where('name', '=', 'delete-products-types')->first()->id],
        ]);
        UserPermission::insert([
            ['user_id' => $user_id, 'permission_id' => Permission::where('name', '=', 'view-discounts')->first()->id],
            ['user_id' => $user_id, 'permission_id' => Permission::where('name', '=', 'add-discounts')->first()->id],
            ['user_id' => $user_id, 'permission_id' => Permission::where('name', '=', 'edit-discounts')->first()->id],
            ['user_id' => $user_id, 'permission_id' => Permission::where('name', '=', 'delete-discounts')->first()->id],
        ]);
        UserPermission::insert([
            ['user_id' => $user_id, 'permission_id' => Permission::where('name', '=', 'add-images')->first()->id],
            ['user_id' => $user_id, 'permission_id' => Permission::where('name', '=', 'delete-images')->first()->id],
        ]);
        UserPermission::insert([
            ['user_id' => $user_id, 'permission_id' => Permission::where('name', '=', 'view-articles')->first()->id],
            ['user_id' => $user_id, 'permission_id' => Permission::where('name', '=', 'add-articles')->first()->id],
            ['user_id' => $user_id, 'permission_id' => Permission::where('name', '=', 'edit-articles')->first()->id],
            ['user_id' => $user_id, 'permission_id' => Permission::where('name', '=', 'delete-articles')->first()->id],
        ]);
    }
}
