<?php

namespace Database\Seeders;

use App\Models\User\UserConfirmCode;
use Illuminate\Database\Seeder;

class UserConfirmCodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserConfirmCode::factory()->count(10)->create();
    }
}
