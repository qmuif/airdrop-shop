<?php

namespace Database\Seeders;

use App\Models\User\RoleUser;
use Illuminate\Database\Seeder;

class RoleUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RoleUser::factory()->count(10)->create();
    }
}
