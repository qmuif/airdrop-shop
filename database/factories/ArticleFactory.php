<?php

namespace Database\Factories;

use App\Models\Article;
use App\Models\Image;
use Illuminate\Database\Eloquent\Factories\Factory;

class ArticleFactory extends Factory
{
    protected $model = Article::class;

    public function definition(): array
    {
    	return [
    	    'title' => $this->faker->realTextBetween(40),
            'body' => $this->faker->realTextBetween(100),
            'image_id' => Image::create([
                'path' => '/public/images/test/news/' . $this->faker->numberBetween(0,3) . ".png",
                'description' => $this->faker->text(20)
            ])
    	];
    }
}
