<?php

namespace Database\Factories;

use App\Models\Discount;
use Illuminate\Database\Eloquent\Factories\Factory;

class DiscountFactory extends Factory
{
    protected $model = Discount::class;

    public function definition(): array
    {
    	return [
            'sum' => $this->faker->numberBetween(1000, 20000),
            'description' => $this->faker->text,
            'from_date' => $this->faker->dateTimeBetween('-30 days'),
            'to_date' => $this->faker->dateTimeBetween('now', '+30 days')
    	];
    }
}
