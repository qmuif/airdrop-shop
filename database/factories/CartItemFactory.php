<?php

namespace Database\Factories;

use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Product\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class CartItemFactory extends Factory
{
    protected $model = CartItem::class;

    public function definition(): array
    {
    	return [
    	    'product_id' => $this->faker->unique->randomElement(Product::all()),
    	    'cart_id' => $this->faker->unique->randomElement(Cart::all()),
            'quantity' => $this->faker->randomDigitNotZero()
    	];
    }
}
