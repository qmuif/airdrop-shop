<?php

namespace Database\Factories;

use App\Models\Image;
use Illuminate\Database\Eloquent\Factories\Factory;

class ImageFactory extends Factory
{
    protected $model = Image::class;

    public function definition(): array
    {
    	return [
    	    'path' => '/public/images/test/'.$this->faker->randomDigit().".png",
            'description' => $this->faker->text(20)
    	];
    }
}
