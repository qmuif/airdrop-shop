<?php

namespace Database\Factories\Product;

use App\Models\Image;
use App\Models\Product\Product;
use App\Models\Product\ProductImages;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductImagesFactory extends Factory
{
    protected $model = ProductImages::class;

    public function definition(): array
    {
        return [
            'product_id' => Product::all()->random(),
            'image_id' => Image::all()->random()
        ];
    }
}
