<?php

namespace Database\Factories\Product;

use App\Models\Discount;
use App\Models\Image;
use App\Models\Product\Product;
use App\Models\Product\ProductStatus;
use App\Models\Product\ProductType;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    protected $model = Product::class;

    protected $iphone_image = [0, 1, 2, 3, 4];
    protected $iphone_names = [
        'Iphone 7',
        'Iphone 7 Plus',
        'Iphone 8',
        'Iphone 8 Plus',
        'Iphone X',
        'Iphone XR',
        'Iphone XS',
        'Iphone 11',
        'Iphone 11 Pro',
        'Iphone 11 Pro Max',
        'Iphone 12',
        'Iphone 12 Mini',
        'Iphone 12 Pro',
        'Iphone 12 Pro Max',
    ];
    protected $macbook_image = [6, 7, 8, 9];
    protected $macbook_names = [
        'MacBook Air 13',
        'MacBook Air 13 M1',
        'MacBook Pro 13',
        'MacBook Pro 13 M1',
        'MacBook Pro 15',
    ];
    protected $memory_sizes = [
        '64gb',
        '128gb',
        '128gb',
        '256gb',
        '512gb',
    ];

    public function definition(): array
    {
        $type = mt_rand(0, 1);
        $price = $this->faker->randomElement([
            39999.00,
            39999.99,
            49999.00,
            59999.00,
            59999.99,
            69999.99,
            69999.00,
            79999.99,
            89999.99,
            99999.99,
            119999.99,
            129999.99,
            139999.99,
            239999.99,
            339999.99,
        ]);
        $discount = Discount::where('sum', '<', $price)->inRandomOrder();
        $discount_id = $discount->exists() ? $discount->first()->id : null;
        return [
            'name' => $this->faker->randomElement($this->getNames($type))." ".$this->faker->randomElement($this->memory_sizes),
            'description' => $this->faker->realTextBetween(40),
            'price' => $price,
            'status_id' => ProductStatus::all()->random(),
            'image_id' => Image::create([
                'path' => '/public/images/test/' . $this->faker->randomElement($this->getImages($type)) . ".png",
                'description' => $this->faker->text(20)
            ]),
            'discount_id' => $this->faker->boolean() ? $discount_id : null,
            'type_id' => ProductType::all()->random(),
        ];
    }

    public function getImages($type)
    {
        return $type ? $this->iphone_image : $this->macbook_image;
    }

    public function getNames($type)
    {
        return $type ? $this->iphone_names : $this->macbook_names;
    }
}
