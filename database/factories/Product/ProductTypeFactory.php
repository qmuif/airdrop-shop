<?php

namespace Database\Factories\Product;

use App\Models\Product\ProductType;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductTypeFactory extends Factory
{
    protected $model = ProductType::class;

    public function definition(): array
    {
    	return [
    	    'name' => $this->faker->randomElement([
    	        'Iphone',
    	        'Ipad',
    	        'IMac',
    	        'MacBook',
    	        'Аксессуары',
            ])
    	];
    }
}
