<?php

namespace Database\Factories\User;

use App\Models\User\Role;
use App\Models\User\RoleUser;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class RoleUserFactory extends Factory
{
    protected $model = RoleUser::class;

    public function definition(): array
    {
        return [
            'user_id' => User::all()->random(),
            'role_id' => Role::all()->random()
        ];
    }
}
