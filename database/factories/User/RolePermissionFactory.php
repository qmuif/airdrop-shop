<?php

namespace Database\Factories\User;

use App\Models\User\RolePermission;
use App\Models\User\Permission;
use App\Models\User\Role;
use Illuminate\Database\Eloquent\Factories\Factory;

class RolePermissionFactory extends Factory
{
    protected $model = RolePermission::class;

    public function definition(): array
    {
    	return [
    	    'role_id' => Role::all()->random()->id,
            'permission_id' => Permission::all()->random()->id
    	];
    }
}
