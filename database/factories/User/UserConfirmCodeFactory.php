<?php

namespace Database\Factories\User;

use App\Models\User\User;
use App\Models\User\UserConfirmCode;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserConfirmCodeFactory extends Factory
{
    protected $model = UserConfirmCode::class;

    public function definition(): array
    {
    	return [
    	    'user_id' => $this->faker->unique->randomElement(User::all()),
    	    'code' => $this->faker->numerify('######')
    	];
    }
}
