<?php

return [
    'locale' => 'ru',
    'faker_locale' => 'ru_RU',

    'aliases' => [
        'App' => Illuminate\Support\Facades\App::class,
    ]
];
