<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/docs', function () use ($router) {
    return view('index.html');
});

$router->group([
    'middleware' => 'cors'
], function ($router) {
    require __DIR__.'/../routes/api/user.php';
    require __DIR__.'/../routes/api/auth.php';
    require __DIR__.'/../routes/api/product.php';
    require __DIR__.'/../routes/api/cart.php';
    require __DIR__.'/../routes/api/discount.php';
    require __DIR__.'/../routes/api/gift_category.php';
    require __DIR__.'/../routes/api/image.php';
    require __DIR__.'/../routes/api/news.php';
});
