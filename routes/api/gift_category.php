<?php

/** @var Router $router */

use Laravel\Lumen\Routing\Router;

$router->get('gift_category', 'GiftCategoriesController@index');
$router->group([
    'middleware' => 'auth'
], function () use ($router) {
    $router->post('gift_category', 'GiftCategoriesController@store');
    $router->patch('gift_category/{id}', 'GiftCategoriesController@update');
    $router->delete('gift_category/{id}', 'GiftCategoriesController@destroy');
});
