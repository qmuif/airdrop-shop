<?php

/** @var Router $router */
use Laravel\Lumen\Routing\Router;

$router->group([
    'middleware' => 'auth'
], function () use ($router) {
    $router->post('image', 'ImageController@store');
});
