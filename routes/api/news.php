<?php

/** @var Router $router */

use Laravel\Lumen\Routing\Router;

$router->get('news', 'ArticleController@index');
$router->get('news/{id}', 'ArticleController@show');

$router->group([
    'middleware' => 'auth'
], function () use ($router) {
    $router->post('news', 'ArticleController@store');
    $router->patch('news/{id}', 'ArticleController@update');
    $router->delete('news/{id}', 'ArticleController@destroy');
});
