<?php

/** @var Router $router */

use Laravel\Lumen\Routing\Router;

$router->group([], function () use ($router) {
    $router->post('login', 'AuthController@login');
    $router->get('me', 'AuthController@me');
    $router->post('refresh', 'AuthController@refresh');
    $router->post('logout', 'AuthController@logout');
});
