<?php

/** @var Router $router */

use Laravel\Lumen\Routing\Router;

$router->group([
    'middleware' => 'auth'
], function () use ($router) {
    $router->get('discounts', 'DiscountController@index');
    $router->post('discounts', 'DiscountController@store');
    $router->patch('discounts/{id}', 'DiscountController@update');
    $router->delete('discounts/{id}', 'DiscountController@destroy');
});
