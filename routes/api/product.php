<?php

/** @var Router $router */

use Laravel\Lumen\Routing\Router;

$router->group([
    'namespace' => 'Product',
], function () use ($router) {
    $router->get('products', 'ProductController@index');
    $router->get('landing', 'ProductController@landing');
    $router->get('products/{id}', 'ProductController@show');
    $router->get('products-types', 'ProductTypeController@index');
    $router->get('products-statuses', 'ProductController@statuses');

    $router->get('product-day', 'ProductDayController@index');

    $router->group([
        'middleware' => 'auth'
    ], function () use ($router) {
        $router->post('products', 'ProductController@store');
        $router->patch('products/{id}', 'ProductController@update');
        $router->delete('products/{id}', 'ProductController@destroy');

        $router->post('products-types', 'ProductTypeController@store');
        $router->patch('products-types/{id}', 'ProductTypeController@update');
        $router->delete('products-types/{id}', 'ProductTypeController@destroy');

        $router->post('products/{product_id}/images/', 'ProductImagesController@store');
        $router->delete('products/{product_id}/images/{image_id}', 'ProductImagesController@destroy');

        $router->post('product-day', 'ProductDayController@store');
    });
});
