<?php

/** @var Router $router */

use Laravel\Lumen\Routing\Router;

$router->group([
    'namespace' => 'Cart',
    'middleware' => ['auth']
], function () use ($router) {
    $router->get('cart', 'CartController@index');
    $router->post('cart/add-item/{product_id}', 'CartItemController@addItem');
    $router->patch('cart/update_item/{product_id}', 'CartItemController@update');
    $router->delete('cart/delete_item/{product_id}', 'CartItemController@destroy');
});
