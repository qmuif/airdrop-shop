<?php

/** @var Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Laravel\Lumen\Routing\Router;

$router->group([
    'namespace' => 'User',
], function () use ($router) {
    $router->post('registration', 'UserController@registration');
    $router->group([
        'middleware' => 'auth'
    ], function () use ($router) {
        $router->get('/user-subscribes', 'UserSubscribesController@index');
        $router->patch('/user-subscribes', 'UserSubscribesController@update');

        $router->post('/confirm-email', 'UserController@confirmEmail');
        $router->get('/users/', 'UserController@index');
        $router->get('/users/{id}', 'UserController@show');
    });
});




