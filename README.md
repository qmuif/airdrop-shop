# Airdrop APP

## API Сервер для интернет-магазина

## Команды для работы с проектом

### Развертывание

```
 в корневой директории проекта: docker-compose up -d
```

### Пересобрать API документацию

```
 docker run %ProjectFolderName%_app php artisan scribe:generate
```


## Структура проекта

```
Навигация по проекту
.
+-- app
|   +-- Exceptions
|   | +-- Handler.php - обработчик всех ошибок
|   +-- Http
|   | +-- Controllers 
|   | +-- Middleware 
|   |  +-- Authenticate.php - обработка авторизации
|   |  +-- ConfirmedUser.php - проверка подтверждения аккаунта
|   |  +-- PermissionMiddleware.php - проверка доступов аккаунта
|   | +-- Resources - API Ресурсы для сущностей
|   +-- Filters - кастомная фильтрация для сущностей
|   +-- Jobs
|   | +-- SendReminderEmail.php - отправка кода подтверждения аккаунта
|   +-- Models - Сущности
|   +-- Providers
|   | +-- UserProvider.php - отслеживаем создание пользователя
|   +-- Traits
|   | +-- HasDownloadImage.php - хелпер для сохранения изображений
|   | +-- HasRolesAndPermissions.php - хелпер для ролей и доступов
+-- bootstrap
|   +-- app.php - точка вохода
+-- config
|   +-- scribe.php - конфиг документации API
+-- database
+-- public
|   +-- docs - документация API
|   |  +-- openapi.yaml
|   +-- swagger - Интерфейс документации
+-- resources
|   +-- lang - Русификация приложения
+-- routes
|   +-- api
+-- tests
|   ... Вспомогателньая конфигурация проекта
```

